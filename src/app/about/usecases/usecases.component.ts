import { Component, OnInit } from '@angular/core'
import { UseCase } from '../usecase.model'

@Component({
  selector: 'app-about-usecases',
  templateUrl: './usecases.component.html',
  styleUrls: ['./usecases.component.scss']
})
export class UsecasesComponent implements OnInit {
  useCases: UseCase[] = [
    {
      id: 'UC-01',
      name: 'Inloggen',
      description: 'Hiermee logt een bestaande gebruiker in.',
      scenario: [
        'Gebruiker vult email en password in en klikt op Login knop.',
        'De applicatie valideert de ingevoerde gegevens.',
        'Indien gegevens correct zijn dan redirect de applicatie naar het startscherm.'
      ],
      actor: 'Reguliere gebruiker',
      precondition: 'Geen',
      postcondition: 'De actor is ingelogd'
    },
    {
      id: 'UC-02',
      name: 'Account Aanmaken',
      description: 'Een gebruiker kan hiermee een account mee aanmaken',
      scenario: [
        'gerbuiker klikt op registreer knop',
        'gebruiker vult zijn/haar gegevens en klikt op registreer knop',
        'De applicatie valideert de ingevoerde gegevens.',
        'Indien gegevens correct zijn dan redirect de applicatie naar het startscherm.'
      ],
      actor: 'Reguliere gebruiker',
      precondition: 'geen',
      postcondition: 'de gebruiker heeft zijn/haar user account angemaakt.'
    },
    {
      id: 'UC-03',
      name: 'Quiz Aanmaken',
      description: 'Een Admin kan hiermee een quiz aanmaken aanmaken',
      scenario: [
        'Admin klikt op quiz aanmaken knop',
        'Admin vult quiz formulier in en klikt op aanmaken knop',
        'De applicatie valideert de ingevoerde gegevens.',
        'Indien gegevens correct zijn dan redirect de applicatie naar het startscherm.'
      ],
      actor: 'Admin',
      precondition: 'Ingelogd als admin',
      postcondition: 'De admin heeft een quiz aangemaakt.'
    }
  ]

  constructor() {}

  ngOnInit() {}
}
